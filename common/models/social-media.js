'use strict';

module.exports = function(SocialMedia) {
	SocialMedia.disableRemoteMethodByName('createChangeStream', true);
	SocialMedia.disableRemoteMethodByName('updateAll', true);

	SocialMedia.observe("before save", function(ctx, next){
		if(ctx.instance){
			ctx.instance.created = new Date();
			ctx.instance.modified = new Date();
			ctx.instance.creatorId = ctx.options.accessToken.userId;
			ctx.instance.modifierId = ctx.options.accessToken.userId;
		} else{
			if(ctx.isNewInstance){
				ctx.data.created = new Date();
				ctx.data.modified = new Date();
				ctx.data.creatorId = ctx.options.accessToken.userId;
				ctx.data.modifierId = ctx.options.accessToken.userId;
			} else{
				ctx.data.modified = new Date();
				ctx.data.modifierId = ctx.options.accessToken.userId;
			}
		}
		next();
	})

	SocialMedia.observe('before delete', function(context, next) {
		var msg = new Error('Deleted');
		msg.statusCode = 200;
		// Soft deleting all data by setting active = 0
		context.Model.updateAll(
			context.where,
			{active: false},
			context.options,
			function(err, data) {
				next(msg);
			}
		);
	});

	SocialMedia.observe('access', function(context, next) {
		if(context.query.where){
			context.query.where = {and:[context.query.where]};
			context.query.where.and.push({active: true});
		}
		else
			context.query.where = {active: true};
		next();
	});


};
