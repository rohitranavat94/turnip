const utils = require('../../utility/utils');
const RECCOMENDATION_TIMEOUT = 300;
module.exports = function(Friend) {
    Friend.disableRemoteMethodByName('createChangeStream', true);
    Friend.disableRemoteMethodByName('updateAll', true);
    Friend.disableRemoteMethodByName('findById', true);

    /**
	 * This method is used to get all friends of a user
	 * @param {username} string 
	 */
    Friend.getFriends = function(username, options, cb) {
        const {User} = Friend.app.models
        var getUserFromUsername = (callback) => {
            User.find({where: {username: username}},options, function(err, data) {callback(err, data)});
        }

        getUserFromUsername(function(err, userData) {
            if(!err && userData && userData.length) {
                var userId = userData[0].id
                if(Friend.app.get('checkUserAuthenticationn') && userId !== options.accessToken.userId) {
                    var error = utils.createError('Authorization error', 'NOT_AUTHORIZED', 401)
                    cb(error)
                } else {
                    Friend.find(
                        {where: {userA: userId}, include: {"relation": "userB", scope: {"fields": ["username"]}}},
                        options,
                        function(err, data) {
                            // Avoiding circular object with prototype functions
                            data = JSON.parse(JSON.stringify(data))
                            if(!err && data){
                                if(data && data.length)
                                    cb(err, data.map(x => x.userB.username))
                                else{
                                    var error = utils.createError('User does not have any friends', 'NO_FRIENDS', 404)
                                    cb(error)
                                }
                            }
                            else {
                                cb(err)
                            }
                        }
                    )
                }
            } else {
                var error = utils.createError('User does not exist', 'USER_NOT_EXIST', 400)
                cb(error)
            }
        })
    }
    Friend.remoteMethod('getFriends', {
        accepts: [
            {arg: 'username', type: 'string'}, 
            {arg: "options", type: "object", http: "optionsFromRequest"}
        ],
        returns: [{arg: 'friends', type: 'array', root: true}],
        http: {path: '/:username', verb: 'GET'},
        description: 'This method returns all friends for a user',

    })

    /**
	 * This method is used to get all friends suggestions of a user
	 * @param {username} string 
	 */
    Friend.getSuggestions = (username, options, cb) => {
        const {User} = Friend.app.models

        var getFriends = async(userIds, notInUserIds) => {
            return Friend.find({where: {userA: {inq: userIds}, userB: {nin: notInUserIds}}},options)
        }

        var getUserFromUsername = (callback) => {
            User.find({where: {username: username}},options, function(err, data) {callback(err, data)});
        }

        var getSuggestionFromCache = (key, cb) => {
            User.app.dataSources.redisStore.getJson(key, function(err, data) {
                cb(err, data)
            })
        }

        getUserFromUsername(function(err, userData) {
            if(!err && userData && userData.length) {
                var userId = userData[0].id
                if(Friend.app.get('checkUserAuthenticationn') && userId !== options.accessToken.userId) {
                    var error = utils.createError('Authorization error', 'NOT_AUTHORIZED', 401)
                    cb(error)
                } else {
                    (async() => {
                        var directFriends = await getFriends([userId], [])
    
                        getSuggestionFromCache(userId, function(err, cachedData) {
                            if(!err && cachedData) {
                                console.log("Getting data from cache: ", cachedData)
                                // Need to add a check to exclude friends made after result was cached.
                                cb(null, {suggestions: cachedData})
                            } else {
                                (async() => {
                                    if(directFriends && directFriends.length) {
                                        try{
                                            var directFriendIds = JSON.parse(JSON.stringify(directFriends)).map(x => x.userB)
                                            console.log("directFriendIds: ", directFriendIds)
                                            var level1Friends = await getFriends(directFriendIds, [userId])
                                            var level1FriendIds = JSON.parse(JSON.stringify(level1Friends)).map(x => x.userB)
                                            console.log("level1FriendIds: ", level1FriendIds)
                                            var level2Friends = await getFriends(level1FriendIds, [...level1FriendIds, ...directFriendIds, userId])
                                            var level2FriendIds = JSON.parse(JSON.stringify(level2Friends)).map(x => x.userB)
                                            console.log("level2FriendIds: ", level2FriendIds)
                                        } catch(e) {
                
                                        }
                
                                        User.find({where: {id: {inq: [...level1FriendIds, level2FriendIds]}}}, options, function(err, data) {
                                            if(!err && data) {
                                                var data = JSON.parse(JSON.stringify(data))
                                                var usernames = data.map(x => x.username)
                                                console.log("Getting data from database: ", usernames)
                                                // Caching recommendation for a certain time. Ideally users might have many recommendation and we will show top 10 recommendation.
                                                // This top 10 can be selected randomly
                                                User.app.dataSources.redisStore.setExJson(userId, usernames, RECCOMENDATION_TIMEOUT)
                                                return cb(null, {suggestions: usernames})
                                            } else {
                                                cb(err)
                                            }
                                        })
                                    }
                                })()
                            }
                        })
                    })()
                }
            } else {
                var error = utils.createError('User does not have any friend suggestion', 'NO_FRIENDS_SUGGESTION', 404)
                cb(error)
            }
        })

    }
    Friend.remoteMethod('getSuggestions', {
        accepts: [
            {arg: 'username', type: 'string'}, 
            {arg: "options", type: "object", http: "optionsFromRequest"}
        ],
        returns: [{arg: 'suggestions', type: 'array', root: true}],
        http: {path: '/suggestions/:username', verb: 'GET'},
        description: 'This method returns all friends for a user',

    })

}