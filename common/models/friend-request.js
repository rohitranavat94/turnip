const utils = require('../../utility/utils');
module.exports = function(FriendRequest) {
    FriendRequest.disableRemoteMethodByName('createChangeStream', true);
    FriendRequest.disableRemoteMethodByName('updateAll', true);
    FriendRequest.disableRemoteMethodByName('findById', true);
    
    FriendRequest.validatesUniquenessOf('sourceUserId', {scopedTo: ['destinationUserId'], message: 'Friend request has already been sent.'});
    
    /**
	 * This method is used to send a friend request from one user to another.
	 * @param {string} sourceUsername User who is sending the request.
     * @param {string} destinationUsername User to who the friend request is sent.
	 */
    FriendRequest.add = async(sourceUsername, destinationUsername, options, cb) => {
        const {Friend, User} = FriendRequest.app.models

        /**
         * 
         * @param {string} username 
         */
        var getUserFromUsername = (username) => {
            return User.find({where: {username: username}}, options)
        }

        var userId = options.accessToken.userId
        var sourceUser = await getUserFromUsername(sourceUsername)
        var destinationUser = await getUserFromUsername(destinationUsername)
        if(sourceUser && !sourceUser.length)
            return cb(utils.createError('Source User does not exist', 'USER_NOT_EXIST', 400))
        if(destinationUser && !destinationUser.length)
            return cb(utils.createError('Destination User does not exist', 'USER_NOT_EXIST', 400))

        var sourceUserId = sourceUser[0].id
        var destinationUserId = destinationUser[0].id

        /**
         * Creates a friend request
         */
        var createFriendRequest = () => {
            return FriendRequest.create(
                {
                    sourceUserId, 
                    destinationUserId
                },
                options,
            )
        }

        var checkIfFriendRequestExists = () => {
            return FriendRequest.findOne({where: {sourceUserId: destinationUserId, destinationUserId: sourceUserId}},options)
        }

        var checkIfFriends = () => {
            return Friend.findOne({where: {userA: sourceUserId, userB: destinationUserId}}, options)
        }

        var createFriend = (userA, userB) => {
            return Friend.create({userA, userB}, options)
        }

        var deleteFriendRequest = (id) => {
            return FriendRequest.deleteById(id, options, function(data, err) {})
        }

        /**
         * Creates two users as friend and delete their pending requests.
         * @param {object} existingFriendRequest 
         */
        var addAsFriendAndDeletePendingRequest = async(existingFriendRequest) => {
            try{
                await createFriend(sourceUserId, destinationUserId)
                await createFriend(destinationUserId, sourceUserId)
            } catch(e) {
                return {status: false, e: e}
            }

            await deleteFriendRequest(existingFriendRequest.id)
            return {status: true}
        }

        if(FriendRequest.app.get('checkUserAuthenticationn') && userId !== sourceUserId) {
           var error = utils.createError('Authorization error', 'NOT_AUTHORIZED', 401)
           cb(error)
        } else {

            // Checking if users are already friends
            var alreadyFriends = await checkIfFriends();
            if(alreadyFriends) {
                var error = utils.createError('Users are already friends', 'ALREADY_FRIENDS', 400)
                cb(error)
            } else {
                // Checking if destination user has already sent a request to source user
                var friendRequestExist = await checkIfFriendRequestExists();
                if(friendRequestExist) {
                    var addAsFriendAndDeletePendingRequestData = await addAsFriendAndDeletePendingRequest(friendRequestExist)
                    if(addAsFriendAndDeletePendingRequestData.status) 
                        cb(null, addAsFriendAndDeletePendingRequestData)
                    else 
                        cb(addAsFriendAndDeletePendingRequestData.e)
                } else {
                    try{
                        await createFriendRequest();
                        cb(null, {status: true})
                    } catch(e) {
                        cb(e)
                    }
                }
            }
        }

    }
    FriendRequest.remoteMethod('add', {
        accepts: [
            {arg: 'sourceUsername', type: 'string'}, 
            {arg: 'destinationUsername', type: 'string'},
            { arg: "options", type: "object", http: "optionsFromRequest"}
        ],
        returns: [{arg: 'status', type: 'boolean', root: true}],
        http: {path: '/add/:sourceUsername/:destinationUsername', status: 202},
        description: 'This method is used to send a friend request',

    })

    /**
	 * This method is used to get all pending friend requests of a user
	 * @param {username} string 
	 */
    FriendRequest.getPendingRequests = function(username, options, cb) {
        const {User} = FriendRequest.app.models
        var getUserFromUsername = (callback) => {
            User.find(
                {where: {username: username}},
                options, 
                function(err, data) {
                    callback(err, data)
                });
        }
        getUserFromUsername(function(err, userData) {
            if(!err && userData && userData.length) {
                var userId = userData[0].id
                if(FriendRequest.app.get('checkUserAuthenticationn') && userId !== options.accessToken.userId) {
                    var error = utils.createError('Authorization error', 'NOT_AUTHORIZED', 401)
                    cb(error)
                } else {
                    FriendRequest.find(
                        {where: {destinationUserId: userId}, include: {"relation": "sourceUser", scope: {"fields": ["username"]}}},
                        options,
                        function(err, data) {
                            // Avoiding circular object with prototype functions
                            data = JSON.parse(JSON.stringify(data))
                            if(!err && data){
                                if(data && data.length)
                                    cb(err, data.map(x => x.sourceUser.username))
                                else{
                                    var error = utils.createError('User does not have any pending friend requests', 'NO_PENDING_FRIEND_REQUEST', 404)
                                    cb(error)
                                }

                            }
                            else
                                cb(err)
                        }
                    )
                }
            } else {
                var error = utils.createError('User does not exist', 'USER_NOT_EXIST', 400)
                cb(error)
            }
        })
    }
    FriendRequest.remoteMethod('getPendingRequests', {
        accepts: [
            {arg: 'username', type: 'string'}, 
            {arg: "options", type: "object", http: "optionsFromRequest"}
        ],
        returns: [{arg: 'friend_requests', type: 'array', root: true}],
        http: {path: '/:username', verb: 'GET'},
        description: 'This method is get all pending requests for a user',

    })
}