'use strict';

module.exports = function(User) {
	User.disableRemoteMethodByName('createChangeStream', true);
	User.disableRemoteMethodByName('updateAll', true);
	User.disableRemoteMethodByName('replaceOrCreate', true);
	const utils = require('../../utility/utils');

	User.observe('before delete', function(context, next) {
		var msg = new Error('Deleted');
		msg.statusCode = 200;
		context.Model.updateAll(
			context.where,
			{active: false},
			function(err, data) {
				next(msg);
			}
		);
	});

	User.observe('access', function(context, next) {
		if(context.query.where){
			context.query.where = {and:[context.query.where]};
			context.query.where.and.push({active: true});
		}
		else
			context.query.where = {active: true};
		next();
	});

	/**
	 * This method is used to check the strength of the password
	 * @param {plain} string 
	 */

	User.validatePassword = function(plain) {
		var err;
		const PASSWORD_VALIDATOR = /(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,16}$/
		if (!plain || typeof plain !== 'string') {
			throw utils.createError('Invalid password.', 'INVALID_PASSWORD', 422);
		}

		if(!PASSWORD_VALIDATOR.test(plain)) {
			throw utils.createError('Password length must be 8 to 16 character, atleast 1 capital letter(A-Z), atleast 1 special character, atleast 1 number', 'PASSWORD_NOT_STRONG', 422);
		}
	}
};
