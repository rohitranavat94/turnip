# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for online assignment given by turnip
* 1.0
* [Assignment Link](https://www.notion.so/Turnip-Assignment-5a6d1ac5f4f842ae9e10358b1b59ad0b)

### How do I get set up? ###

* Install node v8.15.0
* Install mysql v5.17.27
* Install redis v5.0.5
* Start mysql and redis server on port 3306(default) and 6379(default) respectively
* Run the command ```npm install```
* Create a database in mysql with the name `socialMedia`
* Add `username` and `password` for mysql in server/datasources.json and database.json
* Run the command `npm install db-migrate`
* Run the command `db-migrate up`
* Run the command `node .`
* Hit localhost:5000/explorer on your web browser for swagger.

### General guidelines ###

* Create a user by a POST request on /user with params - 'username', 'email', 'firstName', 'lastName', 'password'
* POST /users/login api with params - 'email' and 'password'. This will generate an access token which will be used for all other api requests
* GET /friendRequests/:username to get all pending friend requests.
* POST /friendRequests/add/:sourceUsername/:destinationUsername to send a friend request from one user to another.
* GET /friends/:username to get all friends for a user.
* GET /friends/suggestions/:username to get all the suggestions.
* Authentication method can be enabled/disabled from server/config.json. If enabled, data will be restricted at user level.
* Suggestions are cached with a timeout.


### ToDo ###
* Cache username with their corresponding id.
* Dockerize the entire application.
* Write unit test cases.


### Who do I talk to? ###

* Rohit Ranavat
* rohitranavat94@gmail.com