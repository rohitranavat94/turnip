function createError(message, code, statusCode) {
    var err = new Error(message);
    err.code = code;
    err.status = statusCode;
    return err;
}

exports.createError = createError;