var ds = require('./../datasources.json');
var Redis = require('./../../services/redis');

module.exports = function(app) {
	app.dataSources.redisStore = new Redis(ds.socialMediaRedis);
	app.dataSources.redisStore.connect(function(err, status){
		if(err) {
			console.error(err);
		} else {
            console.log("Redis connection is ", status.status)
        }
	});
}
