'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  var sql=
  `CREATE TABLE user (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    email varchar(200) NOT NULL,
    username varchar(100) DEFAULT NULL,
    firstName varchar(100) DEFAULT NULL,
    lastName varchar(100) DEFAULT NULL,
    emailVerified tinyint(4) DEFAULT '1',
    password varchar(512) DEFAULT NULL,
    active tinyint(1) NOT NULL DEFAULT '1',
    PRIMARY KEY (id),
    UNIQUE email (email),
    UNIQUE username (username)
  ) ENGINE=InnoDB`;
  return db.runSql(sql,[]);
};

exports.down = function(db) {
  var sql=`DROP TABLE user;`;
  return db.runSql(sql,[]);
};

exports._meta = {
  "version": 1
};
