'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  var sql=
  `CREATE TABLE friendRequest (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    sourceUserId bigint(20) NOT NULL,
    destinationUserId bigint(20) NOT NULL,
    status varchar(10),
    active tinyint(1) NOT NULL DEFAULT '1',
    created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    creatorId bigint(20) DEFAULT NULL,
    modified datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifierId bigint(20) DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (sourceUserId) REFERENCES user(id),
    FOREIGN KEY (destinationUserId) REFERENCES user(id)
  ) ENGINE=InnoDB;`;
  return db.runSql(sql,[]);
};

exports.down = function(db) {
  var sql=`DROP TABLE friendRequest;`;
  return db.runSql(sql,[]);
};

exports._meta = {
  "version": 1
};
