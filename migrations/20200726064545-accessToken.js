'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  var sql=
  `CREATE TABLE AccessToken (
    id varchar(255) NOT NULL,
    ttl int(11) NOT NULL,
    created datetime NOT NULL,
    userId bigint(20) NOT NULL,
    scopes varchar(40) DEFAULT NULL,
    PRIMARY KEY (id),
    KEY userId (userId),
    KEY created (created),
    KEY ttl (ttl)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
  return db.runSql(sql,[]);
};

exports.down = function(db) {
  var sql=`DROP TABLE AccessToken;`;
  return db.runSql(sql,[]);
};

exports._meta = {
  "version": 1
};
