var ioredis = require('ioredis');
var hash = require('object-hash');

function Redis(settings) {
	this.settings = settings;
}

Redis.prototype.connect = function(cb) {
	var self = this;
	var connector = new ioredis(self.settings.url ,
		{
			retryStrategy: function () {
				return false;
			}
		}
	);
	connector.on('ready', function(){
		self.connector = connector;
		if(cb) cb(null, connector);
	});
	connector.on('error', function(err){
		console.error(err);
		if(cb) cb(err, null);
	});
}

Redis.prototype.sendCommand = function(cmd,  args, cb) {
	this.connector.sendCommand(
		new ioredis.Command(cmd, args, 'utf-8', function(err, value) {
			if(!err) {
				cb(null, value);
			} else {
				cb(err, null);
			}
		})
	);
}

Redis.prototype.setJson = function(key, value) {
	var self = this;
	self.sendCommand('set', [key, JSON.stringify(value)], function(){

	});
}

Redis.prototype.setExJson = function(key, value, ttl) {
	var self = this;
	self.sendCommand('setex', [key,ttl, JSON.stringify(value)], function(){

	});
}

Redis.prototype.hash = Redis.hash =  function(object) {
	return hash(object);
}

Redis.prototype.getJson = function(key, cb) {
	var self = this;
	self.sendCommand('get', [key], function(err, data){
		if(!err && data) {
			cb(null, JSON.parse(data));
		} else {
			if(err) {
				cb(err, null);
			} else {
				cb(new Error('Not Found'), null);
			}
		}
	});
}

Redis.prototype.addToSet = function(key, value, cb) {
	var self = this;
	self.sendCommand('sadd', [key, value], cb);
}

Redis.prototype.getFromSet = function(key, cb) {
	var self = this;
	self.sendCommand('smembers', [key], cb);
}

Redis.prototype.getKeysByPattern = function(key, cb) {
	var self = this;
	var stream = self.connector.scanStream({
		match: key,
		// returns approximately 100 elements per call
		count: 100
	});

	var keys = [];
	stream.on('data', function (resultKeys) {
		// `resultKeys` is an array of strings representing key names
		for (var i = 0; i < resultKeys.length; i++) {
			keys.push(resultKeys[i]);
		}
	});
	stream.on('end', function () {
		cb(keys)
	});
}

Redis.prototype.deleteKeysByPattern = function(key, cb) {
	var self = this;
	var stream = self.connector.scanStream({
		match: key,
		// returns approximately 100 elements per call
		count: 100
	});

	var keys = [];
	stream.on('data', function (resultKeys) {
		// `resultKeys` is an array of strings representing key names
		for (var i = 0; i < resultKeys.length; i++) {
			keys.push(resultKeys[i]);
		}
	});
	stream.on('end', function () {
		if(keys.length) {
			self.connector.del(keys);
		}
		cb(keys);
	});
}

Redis.prototype.batchDeleteKeysByPattern = function(key, cb) {
	var self = this;
	var stream = self.connector.scanStream({
		match: key,
		// returns approximately 100 elements per call
		count: 100
	});

	stream.on('data', function (resultKeys) {
        if (resultKeys.length) {
            self.connector.del(resultKeys);
        }
    });
	stream.on('end', function () {
		cb();
	});
}

module.exports = Redis;
